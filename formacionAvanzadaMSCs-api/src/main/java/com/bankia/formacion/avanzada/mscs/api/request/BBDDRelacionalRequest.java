package com.bankia.formacion.avanzada.mscs.api.request;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BBDDRelacionalRequest implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonProperty(value = "bkOffsetTj", required = false)
	private String bkOffsetTj = null;
	
	@JsonProperty(value = "bkTipoTj", required = true)
	@NotNull
	@Size(max = 2)
	private String bkTipoTj = null;
	
	@JsonProperty(value = "bkProductoPersTj", required = true)
	@NotNull
	private String bkProductoPersTj = null;


	@JsonProperty(value = "bkEstadoTj", required = false)

	@Size(max = 1)
	private String bkEstadoTj = null;
	
	@JsonProperty(value = "fecha", required = true)
	@NotNull
	private Date fecha = null;
	
	@JsonProperty(value = "hora", required = true)
	@NotNull
	private String hora = null;
	
	@JsonProperty(value = "pan", required = true)
	@NotNull
	@Size(max = 19)
	private String pan = null;
	
	@JsonProperty(value = "expDate", required = true)
	@NotNull
	@Size(max = 4)
	private String expDate = null;

	
	@JsonProperty(value = "purchaseAmount", required = true)
	@NotNull
	@Size(max = 48)
	private String purchaseAmount = null;
	
	@JsonProperty(value = "purchaseCurrency", required = true)
	@NotNull
    @Size(max = 3)
	private String purchaseCurrency = null;
	
	@JsonProperty(value = "merchantName", required = true)
	@NotNull
	@Size(max = 40)
	private String merchantName = null;
	
	@JsonProperty(value = "operationId", required = true)
	@NotNull
	@Size(max = 36)
	private String operationId = null;
	
	@JsonProperty(value = "application", required = true)
	@NotNull
	@Size(max = 20)
	private String application = null;

	public String getBkOffsetTj() {
		return bkOffsetTj;
	}

	public void setBkOffsetTj(String bkOffsetTj) {
		this.bkOffsetTj = bkOffsetTj;
	}

	public String getBkTipoTj() {
		return bkTipoTj;
	}

	public void setBkTipoTj(String bkTipoTj) {
		this.bkTipoTj = bkTipoTj;
	}

	public String getBkProductoPersTj() {
		return bkProductoPersTj;
	}

	public void setBkProductoPersTj(String bkProductoPersTj) {
		this.bkProductoPersTj = bkProductoPersTj;
	}

	public String getBkEstadoTj() {
		return bkEstadoTj;
	}

	public void setBkEstadoTj(String bkEstadoTj) {
		this.bkEstadoTj = bkEstadoTj;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public String getExpDate() {
		return expDate;
	}

	public void setExpDate(String expDate) {
		this.expDate = expDate;
	}


	public String getPurchaseAmount() {
		return purchaseAmount;
	}

	public void setPurchaseAmount(String purchaseAmount) {
		this.purchaseAmount = purchaseAmount;
	}

	public String getPurchaseCurrency() {
		return purchaseCurrency;
	}

	public void setPurchaseCurrency(String purchaseCurrency) {
		this.purchaseCurrency = purchaseCurrency;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public String getOperationId() {
		return operationId;
	}

	public void setOperationId(String operationId) {
		this.operationId = operationId;
	}

	public String getApplication() {
		return application;
	}

	public void setApplication(String application) {
		this.application = application;
	}
}
