package com.bankia.formacion.avanzada.mscs.api.response;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_EMPTY)
public class BBDDRelacionalResponse implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty(value = "bkOffsetTj", required = false)
	private String bkOffsetTj = null;
	
	@JsonProperty(value = "bkTipoTj", required = true)
	private String bkTipoTj = null;
	
	@JsonProperty(value = "bkProductoPersTj", required = true)
	private String bkProductoPersTj = null;

	@JsonProperty(value = "bkEstadoTj", required = false)
	private String bkEstadoTj = null;
	
	@JsonProperty(value = "fecha", required = true)
	private Date fecha = null;
	
	@JsonProperty(value = "hora", required = true)
	private String hora = null;
	
	@JsonProperty(value = "pan", required = true)
	private String pan = null;
	
	@JsonProperty(value = "expDate", required = true)
	private String expDate = null;
	
	
	@JsonProperty(value = "purchaseAmount", required = true)
	private String purchaseAmount = null;
	
	@JsonProperty(value = "purchaseCurrency", required = true)
	private String purchaseCurrency = null;
	
	@JsonProperty(value = "merchantName", required = true)
	private String merchantName = null;
	
	@JsonProperty(value = "operationId", required = true)
	private String operationId = null;
	
	@JsonProperty(value = "application", required = true)
	private String application = null;
	
	@JsonProperty(value = "fechaSolicitud", required = true)
	private Date fechaSolicitud;
	
	@JsonProperty(value = "finalizado", required = true)
	private Character finalizado;
	
	@JsonProperty(value = "tipoSolicitud", required = true)
	private Character tipoSolicitud;

	public String getBkOffsetTj() {
		return bkOffsetTj;
	}

	public void setBkOffsetTj(String bkOffsetTj) {
		this.bkOffsetTj = bkOffsetTj;
	}

	public String getBkTipoTj() {
		return bkTipoTj;
	}

	public void setBkTipoTj(String bkTipoTj) {
		this.bkTipoTj = bkTipoTj;
	}

	public String getBkProductoPersTj() {
		return bkProductoPersTj;
	}

	public void setBkProductoPersTj(String bkProductoPersTj) {
		this.bkProductoPersTj = bkProductoPersTj;
	}

	public String getBkEstadoTj() {
		return bkEstadoTj;
	}

	public void setBkEstadoTj(String bkEstadoTj) {
		this.bkEstadoTj = bkEstadoTj;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public String getExpDate() {
		return expDate;
	}

	public void setExpDate(String expDate) {
		this.expDate = expDate;
	}


	public String getPurchaseAmount() {
		return purchaseAmount;
	}

	public void setPurchaseAmount(String purchaseAmount) {
		this.purchaseAmount = purchaseAmount;
	}

	public String getPurchaseCurrency() {
		return purchaseCurrency;
	}

	public void setPurchaseCurrency(String purchaseCurrency) {
		this.purchaseCurrency = purchaseCurrency;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public String getOperationId() {
		return operationId;
	}

	public void setOperationId(String operationId) {
		this.operationId = operationId;
	}

	public String getApplication() {
		return application;
	}

	public void setApplication(String application) {
		this.application = application;
	}

	public Date getFechaSolicitud() {
		return fechaSolicitud;
	}

	public void setFechaSolicitud(Date fechaSolicitud) {
		this.fechaSolicitud = fechaSolicitud;
	}

	public Character getFinalizado() {
		return finalizado;
	}

	public void setFinalizado(Character finalizado) {
		this.finalizado = finalizado;
	}

	public Character getTipoSolicitud() {
		return tipoSolicitud;
	}

	public void setTipoSolicitud(Character tipoSolicitud) {
		this.tipoSolicitud = tipoSolicitud;
	}
	
	
}
