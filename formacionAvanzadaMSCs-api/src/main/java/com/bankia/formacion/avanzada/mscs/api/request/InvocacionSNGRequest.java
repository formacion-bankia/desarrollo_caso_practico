package com.bankia.formacion.avanzada.mscs.api.request;

public class InvocacionSNGRequest {
	
	private String identificadorMensaje;
	
	private Character indicadorBusquedaDatosBasicos;
	
	private String identificadorCliente;
	
	private Character indicadorEnvioSMS;
	
	private Character indicadorEnvioEmail;
	
	private Character indicadorEnvioPush;
	
	private Character indicadorPublicidad;
	
	private String codigoIdioma;
	
	private Character indicadorEnviarAdjunto;
	
	private String identificadorOperacion;

	public String getIdentificadorMensaje() {
		return identificadorMensaje;
	}

	public void setIdentificadorMensaje(String identificadorMensaje) {
		this.identificadorMensaje = identificadorMensaje;
	}

	public Character getIndicadorBusquedaDatosBasicos() {
		return indicadorBusquedaDatosBasicos;
	}

	public void setIndicadorBusquedaDatosBasicos(
			Character indicadorBusquedaDatosBasicos) {
		this.indicadorBusquedaDatosBasicos = indicadorBusquedaDatosBasicos;
	}

	public String getIdentificadorCliente() {
		return identificadorCliente;
	}

	public void setIdentificadorCliente(String identificadorCliente) {
		this.identificadorCliente = identificadorCliente;
	}

	public Character getIndicadorEnvioSMS() {
		return indicadorEnvioSMS;
	}

	public void setIndicadorEnvioSMS(Character indicadorEnvioSMS) {
		this.indicadorEnvioSMS = indicadorEnvioSMS;
	}

	public Character getIndicadorEnvioEmail() {
		return indicadorEnvioEmail;
	}

	public void setIndicadorEnvioEmail(Character indicadorEnvioEmail) {
		this.indicadorEnvioEmail = indicadorEnvioEmail;
	}



	public Character getIndicadorPublicidad() {
		return indicadorPublicidad;
	}

	public void setIndicadorPublicidad(Character indicadorPublicidad) {
		this.indicadorPublicidad = indicadorPublicidad;
	}

	public String getCodigoIdioma() {
		return codigoIdioma;
	}

	public void setCodigoIdioma(String codigoIdioma) {
		this.codigoIdioma = codigoIdioma;
	}

	public Character getIndicadorEnviarAdjunto() {
		return indicadorEnviarAdjunto;
	}

	public void setIndicadorEnviarAdjunto(Character indicadorEnviarAdjunto) {
		this.indicadorEnviarAdjunto = indicadorEnviarAdjunto;
	}

	public String getIdentificadorOperacion() {
		return identificadorOperacion;
	}

	public void setIdentificadorOperacion(String identificadorOperacion) {
		this.identificadorOperacion = identificadorOperacion;
	}

	public Character getIndicadorEnvioPush() {
		return indicadorEnvioPush;
	}

	public void setIndicadorEnvioPush(Character indicadorEnvioPush) {
		this.indicadorEnvioPush = indicadorEnvioPush;
	}
	
	
	
	
}
