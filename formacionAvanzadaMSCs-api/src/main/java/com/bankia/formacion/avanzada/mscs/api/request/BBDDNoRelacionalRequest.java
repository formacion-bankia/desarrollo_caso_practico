package com.bankia.formacion.avanzada.mscs.api.request;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BBDDNoRelacionalRequest {

	@JsonProperty(value = "numTarjeta", required = true)
	@Size(max = 16)
	@NotNull
	private String numTarjeta;

	@JsonProperty(value = "idCliente", required = false)
	@NotNull
	private String idCliente;

	@JsonProperty(value = "numContrato", required = false)
	@NotNull
	private String numContrato;

	@JsonProperty(value = "numDocumento", required = false)
	@NotNull
	private String numDocumento;

	public String getNumTarjeta() {
		return numTarjeta;
	}

	public void setNumTarjeta(String numTarjeta) {
		this.numTarjeta = numTarjeta;
	}

	public String getIdCliente() {
		return idCliente;
	}

	public String getNumDocumento() {
		return numDocumento;
	}

	public void setNumDocumento(String numDocumento) {
		this.numDocumento = numDocumento;
	}

	public void setIdCliente(String idCliente) {
		this.idCliente = idCliente;
	}

	public String getNumContrato() {
		return numContrato;
	}

	public void setNumContrato(String numContrato) {
		this.numContrato = numContrato;
	}

}
