package com.bankia.formacion.avanzada.mscs.api.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InvocacionMSCResponse implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("mensajeAutenticacion")
	private String mensajeAutenticacion = null;
	
	@JsonProperty("resultadoAutenticacion")
	private String resultadoAutenticacion = null;
	
	@JsonProperty("operacion")
	private String operacion = null;
	
	@JsonProperty("posicion1")
	private String posicion1 = null;
	
	@JsonProperty("posicion2")
	private String posicion2 = null;
	
	@JsonProperty("sumando")
	private String sumando = null;

	public String getMensajeAutenticacion() {
		return mensajeAutenticacion;
	}

	public void setMensajeAutenticacion(String mensajeAutenticacion) {
		this.mensajeAutenticacion = mensajeAutenticacion;
	}

	public String getResultadoAutenticacion() {
		return resultadoAutenticacion;
	}

	public void setResultadoAutenticacion(String resultadoAutenticacion) {
		this.resultadoAutenticacion = resultadoAutenticacion;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public String getPosicion1() {
		return posicion1;
	}

	public void setPosicion1(String posicion1) {
		this.posicion1 = posicion1;
	}

	public String getPosicion2() {
		return posicion2;
	}

	public void setPosicion2(String posicion2) {
		this.posicion2 = posicion2;
	}

	public String getSumando() {
		return sumando;
	}

	public void setSumando(String sumando) {
		this.sumando = sumando;
	}
	
	
}
