package com.bankia.formacion.avanzada.mscs.api.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InvocacionSNGResponse implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("identificadorMensajeEnviado")
	private String identificadorMensajeEnviado = null;

	public String getIdentificadorMensajeEnviado() {
		return identificadorMensajeEnviado;
	}

	public void setIdentificadorMensajeEnviado(String identificadorMensajeEnviado) {
		this.identificadorMensajeEnviado = identificadorMensajeEnviado;
	}
	
	
}
