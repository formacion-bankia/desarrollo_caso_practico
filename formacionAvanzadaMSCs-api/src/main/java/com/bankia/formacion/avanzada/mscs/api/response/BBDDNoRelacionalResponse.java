package com.bankia.formacion.avanzada.mscs.api.response;

import java.io.Serializable;

public class BBDDNoRelacionalResponse implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String numTarjeta;
	
	private String idCliente;
	
	private String numContrato;
	
	private String numDcumento;

	public String getNumTarjeta() {
		return numTarjeta;
	}

	public void setNumTarjeta(String numTarjeta) {
		this.numTarjeta = numTarjeta;
	}

	public String getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(String idCliente) {
		this.idCliente = idCliente;
	}

	public String getNumContrato() {
		return numContrato;
	}

	public void setNumContrato(String numContrato) {
		this.numContrato = numContrato;
	}

	public String getNumDcumento() {
		return numDcumento;
	}

	public void setNumDcumento(String numDcumento) {
		this.numDcumento = numDcumento;
	}
	
	
	
}
