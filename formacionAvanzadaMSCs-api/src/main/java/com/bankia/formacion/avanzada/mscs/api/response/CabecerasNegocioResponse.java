package com.bankia.formacion.avanzada.mscs.api.response;

import java.io.Serializable;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_EMPTY)
public class CabecerasNegocioResponse implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Map<String, String> cabecerasNegocio;

	public Map<String, String> getCabecerasNegocio() {
		return cabecerasNegocio;
	}

	public void setCabecerasNegocio(Map<String, String> cabecerasNegocio) {
		this.cabecerasNegocio = cabecerasNegocio;
	}


	
	
}
