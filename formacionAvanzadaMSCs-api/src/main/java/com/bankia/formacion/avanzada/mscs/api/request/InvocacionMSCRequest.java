package com.bankia.formacion.avanzada.mscs.api.request;

public class InvocacionMSCRequest {
	private String fechaCaducidad;
	
	private String nombreComercio;
	
	private String offsetPan;
	
	private String idOperacion;
	
	private String pan;
	
	private String importeOperacion;
	
	private String monedaOperacion;

	public String getFechaCaducidad() {
		return fechaCaducidad;
	}

	public void setFechaCaducidad(String fechaCaducidad) {
		this.fechaCaducidad = fechaCaducidad;
	}

	public String getNombreComercio() {
		return nombreComercio;
	}

	public void setNombreComercio(String nombreComercio) {
		this.nombreComercio = nombreComercio;
	}

	public String getOffsetPan() {
		return offsetPan;
	}

	public void setOffsetPan(String offsetPan) {
		this.offsetPan = offsetPan;
	}

	public String getIdOperacion() {
		return idOperacion;
	}

	public void setIdOperacion(String idOperacion) {
		this.idOperacion = idOperacion;
	}

	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public String getImporteOperacion() {
		return importeOperacion;
	}

	public void setImporteOperacion(String importeOperacion) {
		this.importeOperacion = importeOperacion;
	}

	public String getMonedaOperacion() {
		return monedaOperacion;
	}

	public void setMonedaOperacion(String monedaOperacion) {
		this.monedaOperacion = monedaOperacion;
	}
	
	
}
