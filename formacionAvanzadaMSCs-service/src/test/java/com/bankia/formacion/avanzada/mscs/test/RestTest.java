package com.bankia.formacion.avanzada.mscs.test;

import static org.junit.Assert.assertTrue;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.bankia.aq.msa.common.api.BaseResponse;

//@RunWith(SpringRunner.class)
//@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
public class RestTest {

/**	// Se deben integrar los POJOS proporcionados por Arquitectura para su funcionamiento
	@Autowired
    private TestRestTemplate restTemplate;
	
	private static final String URI = "/formacionAvanzada/${serviceVersion}/myCustomPath";

	@Test
	@Ignore
	public void testGet() {
		
		ResponseEntity<BaseResponse<FormacionAvanzadaResponse>> response;
		response = restTemplate.exchange(
				URI, 
		        HttpMethod.GET, 
		        getDefaultEntity(), 
		        new ParameterizedTypeReference<BaseResponse<FormacionAvanzadaResponse>>() {}
		        );

		assertTrue(response.getStatusCode().is2xxSuccessful());
		assertTrue(!response.getBody().getParametrosSalida().getParam1().isEmpty());
		assertTrue(!response.getBody().getParametrosSalida().getParam2().isEmpty());
	}
	
	@Test
	@Ignore
	public void testPost() {
		
		FormacionAvanzadaRequest request = new FormacionAvanzadaRequest();
		HttpEntity<FormacionAvanzadaRequest> entity = new HttpEntity<>(request, getDefaultHeaders());

		ResponseEntity<BaseResponse<FormacionAvanzadaResponse>> response;
		response = restTemplate.exchange(
				URI, 
		        HttpMethod.POST, 
		        entity, 
		        new ParameterizedTypeReference<BaseResponse<FormacionAvanzadaResponse>>() {}
		        );
		
		assertTrue(response.getStatusCode().is2xxSuccessful());
		assertTrue(!response.getBody().getParametrosSalida().getParam1().isEmpty());
		assertTrue(!response.getBody().getParametrosSalida().getParam2().isEmpty());
	}

	@Test
	@Ignore
	public void testPut() {
		
		FormacionAvanzadaRequest request = new FormacionAvanzadaRequest();
		HttpEntity<FormacionAvanzadaRequest> entity = new HttpEntity<>(request, getDefaultHeaders());

		ResponseEntity<BaseResponse<FormacionAvanzadaResponse>> response;
		response = restTemplate.exchange(
				URI, 
		        HttpMethod.PUT, 
		        entity, 
		        new ParameterizedTypeReference<BaseResponse<FormacionAvanzadaResponse>>() {}
		        );
		
		assertTrue(response.getStatusCode().is2xxSuccessful());
		assertTrue(!response.getBody().getParametrosSalida().getParam1().isEmpty());
		assertTrue(!response.getBody().getParametrosSalida().getParam2().isEmpty());
	}

	@Test
	@Ignore
	public void testDelete() {
		
		ResponseEntity<BaseResponse<FormacionAvanzadaResponse>> response;
		response = restTemplate.exchange(
				URI + "/3495867", 
		        HttpMethod.DELETE, 
		        getDefaultEntity(), 
		        new ParameterizedTypeReference<BaseResponse<FormacionAvanzadaResponse>>() {}
		        );
		
		assertTrue(response.getStatusCode().is2xxSuccessful());
		assertTrue(!response.getBody().getParametrosSalida().getParam1().isEmpty());
		assertTrue(!response.getBody().getParametrosSalida().getParam2().isEmpty());	
	}
	
	private HttpEntity<?> getDefaultEntity(){
		return new HttpEntity<>(getDefaultHeaders());
	}
	
	private HttpHeaders getDefaultHeaders() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
		return headers;
	} */
}
