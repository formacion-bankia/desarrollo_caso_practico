package com.bankia.formacion.avanzada.mscs.mongo.reporitories;

import java.util.List;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.bankia.formacion.avanzada.mscs.api.request.BBDDNoRelacionalRequest;
import com.bankia.formacion.avanzada.mscs.mongo.entities.CatalogoEntity;
import com.bankia.formacion.avanzada.mscs.mongo.entities.TarjetaClienteEntity;
import com.bankia.formacion.avanzada.mscs.mongo.entities.TarjetaClienteFlipEntity;
import com.bankia.formacion.avanzada.mscs.mongo.entities.TarjetaClienteFlopEntity;

@Service
public class TarjetaClienteRepository {
	 private String maestra = "RQCTARJETA_CLIENTE_V1";
	  private MongoTemplate mongoTemplate = null;
	  private int idLog;
	  
	  public TarjetaClienteRepository(MongoTemplate mongoTemplate){
	    this.mongoTemplate = mongoTemplate;
	    this.idLog = valorIdlog();
		
	  }
	  
	  public void insert(TarjetaClienteEntity tarjetaClienteEntity){
		  TarjetaClienteEntity tarjetaClienteEntityImpl = valorIdlog() == 0 ? new TarjetaClienteFlipEntity(tarjetaClienteEntity): new TarjetaClienteFlopEntity(tarjetaClienteEntity) ;
		  
		  this.mongoTemplate.insert(tarjetaClienteEntityImpl);
	  }
	  

	public List<TarjetaClienteEntity> findByTarjeta(String tarjeta)
	  {
	    PageRequest pageable = new PageRequest(0, 100);
	    Query query = new Query();
	    query.addCriteria(Criteria.where("nu_tarjeta").is(tarjeta));
	    query.with(pageable);
	    if(valorIdlog() == 0) {
	    	return (List<TarjetaClienteEntity>)(List<?>) this.mongoTemplate.find(query, TarjetaClienteFlipEntity.class);
	    } else {
	    	return (List<TarjetaClienteEntity>)(List<?>) this.mongoTemplate.find(query, TarjetaClienteFlopEntity.class);
	    }
	  }

	  private int valorIdlog()
	  {
	    Query queryCatalogo = new Query();
	    queryCatalogo.addCriteria(Criteria.where("maestra").is(this.maestra));
	    CatalogoEntity catalogo = (CatalogoEntity)this.mongoTemplate.findOne(queryCatalogo, CatalogoEntity.class);
	    this.idLog = catalogo.getIdlog();
	    return this.idLog;
	  }

	public List<TarjetaClienteEntity> findByUsrus(String ursus) {
	    PageRequest pageable = new PageRequest(0, 100);
	    Query query = new Query();
	    query.addCriteria(Criteria.where("co_persona").is(ursus));
	    query.with(pageable);
	    if(valorIdlog() == 0) {
	    	return (List<TarjetaClienteEntity>)(List<?>) this.mongoTemplate.find(query, TarjetaClienteFlipEntity.class);
	    } else {
	    	return (List<TarjetaClienteEntity>)(List<?>) this.mongoTemplate.find(query, TarjetaClienteFlopEntity.class);
	    }
	  }
}
