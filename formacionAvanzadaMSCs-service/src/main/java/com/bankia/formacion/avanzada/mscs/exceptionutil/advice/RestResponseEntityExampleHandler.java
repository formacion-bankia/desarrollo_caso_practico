package com.bankia.formacion.avanzada.mscs.exceptionutil.advice;

import java.lang.reflect.Field;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.messaging.handler.annotation.support.MethodArgumentNotValidException;
import org.springframework.util.ReflectionUtils;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.bankia.aq.msa.common.api.BaseResponse;
import com.bankia.aq.msa.common.api.ExceptionMessage;
import com.bankia.formacion.avanzada.mscs.exceptionutil.exceptions.BaseException;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;

@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class RestResponseEntityExampleHandler {
	
	
	private static final Logger log = LoggerFactory.getLogger(RestResponseEntityExampleHandler.class);
	
//	@Autowired
//	@Qualifier("appMessageSource")
//	private MessageSource messageSource;
	
	@ExceptionHandler(value = { BaseException.class })
	protected ResponseEntity<BaseResponse<?>> handleServicioException(BaseException ex) {
		log.error(ex.getMessage());
		return ResponseEntity.status(obtenerStatus(ex)).body(obtenerBaseResponseServicioException(ex));
	}
	
	@ExceptionHandler(value = { Throwable.class })
	protected ResponseEntity<BaseResponse<?>> handleException(Throwable ex) {
		log.error(ex.getMessage());
		return ResponseEntity.status(obtenerStatus(ex)).body(obtenerBaseResponse(ex));
	}

	// Las excepciones ajenas al micro pueden ser manejadas de la misma manera
	
	@ExceptionHandler(value = { MissingServletRequestParameterException.class })
	protected ResponseEntity<BaseResponse<?>> handleMethodMissingServletRequestParameterException(MissingServletRequestParameterException ex) {
		log.error(ex.getMessage());
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(obtenerBaseResponse(ex));
	}
	
	@ExceptionHandler(value = { MethodArgumentNotValidException.class })
	protected ResponseEntity<BaseResponse<?>> handleMethodArgumentNotValidException(MethodArgumentNotValidException ex) {
		log.error(ex.getMessage());
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(obtenerBaseResponse(ex));
	}
	
	@ExceptionHandler(value = { HttpMessageNotReadableException.class })
	protected ResponseEntity<BaseResponse<?>> handleHttpMessageNotReadableException(HttpMessageNotReadableException ex) {
		log.error(ex.getMessage());
		Throwable cause = ex.getCause();
		
		if (cause instanceof InvalidFormatException) {
			InvalidFormatException iex = InvalidFormatException.class.cast(cause);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(obtenerBaseResponse(iex));
		} else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(obtenerBaseResponse("El contenido de la petición no es correcto"));
		}
	}
	
	private String obtenerNombreCampo(String mensajeError) {
		final Pattern pattern = Pattern.compile("\\[\\\"(.*?)\\\"\\]");
		final Matcher matcher = pattern.matcher(mensajeError);
		
		StringBuilder builder = new StringBuilder();
		while (matcher.find()) {
			builder.append(matcher.group(1));
			builder.append(".");	
		}
		
		return StringUtils.removeEnd(builder.toString(), ".");
	}

	private BaseResponse<?> obtenerBaseResponse(MethodArgumentNotValidException ex) {
		List<ObjectError> errors = ex.getBindingResult().getAllErrors();
		StringBuilder errores = new StringBuilder();
		for (ObjectError objectError : errors) {
			Field field = ReflectionUtils.findField(objectError.getClass(), "field");
			ReflectionUtils.makeAccessible(field);
			ReflectionUtils.getField(field, objectError);
			errores.append(StringUtils.join(ReflectionUtils.getField(field, objectError), ": ", objectError.getDefaultMessage(), "; "));
		}
		
		BaseResponse<?> baseResponse = new BaseResponse<>();
		ExceptionMessage exceptionmessage = new ExceptionMessage();
		exceptionmessage.setMessage(errores.toString());
		
		baseResponse.setException(exceptionmessage);
		
		return baseResponse;
	}

	private BaseResponse<?> obtenerBaseResponseServicioException(BaseException ex) {
		BaseResponse<?> baseResponse = new BaseResponse<>();
		baseResponse.setException(ex.getExceptionMessage());
		
		return baseResponse;
	}

	private BaseResponse<?> obtenerBaseResponse(InvalidFormatException ex) {
		
		String nombreCampo = obtenerNombreCampo(ex.getLocalizedMessage());
		String mensajeError = StringUtils.join(nombreCampo, ": ", "formato incorrecto");
		
		BaseResponse<?> baseResponse = new BaseResponse<>();
		ExceptionMessage exceptionmessage = new ExceptionMessage();
		exceptionmessage.setMessage(mensajeError);
		
		baseResponse.setException(exceptionmessage);
		
		return baseResponse;
	}

	private BaseResponse<?> obtenerBaseResponse(MissingServletRequestParameterException ex) {
		String nombreCampo = ex.getParameterName();
		
		BaseResponse<?> baseResponse = new BaseResponse<>();
		ExceptionMessage exceptionmessage = new ExceptionMessage();
		
		baseResponse.setException(exceptionmessage);
		
		return baseResponse;
	}
	
	private BaseResponse<?> obtenerBaseResponse(Throwable ex) {
		BaseResponse<?> baseResponse = new BaseResponse<>();
		ExceptionMessage exceptionmessage = new ExceptionMessage();
		exceptionmessage.setMessage(ex.getMessage());
		
		baseResponse.setException(exceptionmessage);
		
		return baseResponse;
	}
	
	private BaseResponse<?> obtenerBaseResponse(String mensaje) {
		BaseResponse<?> baseResponse = new BaseResponse<>();
		ExceptionMessage exceptionmessage = new ExceptionMessage();
		exceptionmessage.setMessage(mensaje);
		
		baseResponse.setException(exceptionmessage);
		
		return baseResponse;
	}

	private HttpStatus obtenerStatus(Throwable ex) {
		HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
		ResponseStatus responseStatus = AnnotationUtils.findAnnotation(ex.getClass(), ResponseStatus.class);
		
		if (null != responseStatus) {
			status = responseStatus.value();
		}
		return status;
	}
}
