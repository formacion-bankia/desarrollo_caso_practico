package com.bankia.formacion.avanzada.mscs.exceptionutil.exceptions.extended;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.bankia.formacion.avanzada.mscs.exceptionutil.exceptions.BaseException;

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
public class MyCustomConflictException extends BaseException{

	private static final long serialVersionUID = 6066979014540469140L;
	private static final String errorCode = HttpStatus.CONFLICT.toString().concat(": ERROR DEFINIDO");
	
	
	public MyCustomConflictException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	
}
