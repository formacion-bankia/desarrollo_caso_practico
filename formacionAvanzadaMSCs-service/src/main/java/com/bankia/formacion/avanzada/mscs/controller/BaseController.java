package com.bankia.formacion.avanzada.mscs.controller;

import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("${bankia.base-path}")
abstract class BaseController {

    protected static final String MDC_TRACKING_TAG = "TRACKING";

}
