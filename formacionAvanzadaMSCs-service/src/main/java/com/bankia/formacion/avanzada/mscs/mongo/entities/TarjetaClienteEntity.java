package com.bankia.formacion.avanzada.mscs.mongo.entities;

public class TarjetaClienteEntity {
	public TarjetaClienteEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TarjetaClienteEntity(String nu_tarjeta, String co_persona, String nu_contrato,
			String nu_documento) {
		super();
		this.nu_tarjeta = nu_tarjeta;
		this.co_persona = co_persona;
		this.nu_contrato = nu_contrato;
		this.nu_documento = nu_documento;
	}
	
	public TarjetaClienteEntity(TarjetaClienteEntity tarjetaClienteEntity) {
		super();
		this.nu_tarjeta = tarjetaClienteEntity.getNu_tarjeta();
		this.co_persona = tarjetaClienteEntity.getCo_persona();
		this.nu_contrato = tarjetaClienteEntity.getNu_contrato();
		this.nu_documento = tarjetaClienteEntity.getNu_documento();
	}

	private String nu_tarjeta;
	
	private String co_persona;
	
	private String nu_contrato;
	
	private String nu_documento;

	@Override
	public String toString() {
		return "CtaAhorroVistaCliente [nu_tarjeta=" + nu_tarjeta + ", co_persona=" + co_persona
				+ ", nu_contrato=" + nu_contrato + ", nu_documento=" + nu_documento + "]";
	}

	public String getNu_tarjeta() {
		return nu_tarjeta;
	}

	public void setNu_tarjeta(String nu_tarjeta) {
		this.nu_tarjeta = nu_tarjeta;
	}

	public String getCo_persona() {
		return co_persona;
	}

	public void setCo_persona(String co_persona) {
		this.co_persona = co_persona;
	}

	public String getNu_contrato() {
		return nu_contrato;
	}

	public void setNu_contrato(String nu_contrato) {
		this.nu_contrato = nu_contrato;
	}

	public String getNu_documento() {
		return nu_documento;
	}

	public void setNu_documento(String nu_documento) {
		this.nu_documento = nu_documento;
	}
	  
}
