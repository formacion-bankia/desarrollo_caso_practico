package com.bankia.formacion.avanzada.mscs.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;

@Configuration
@EnableMongoRepositories(basePackages = "com.bankia.formacion.avanzada.mscs.mongo.reporitories")
@PropertySource("classpath:mongo-database.properties") 
public class MongoDatabaseConfiguration {
	
	@Value("${spring.data.mongodb.host}")
	private String host;

	@Value("${spring.data.mongodb.database}")
	private String database;
	
	@Value("${spring.data.mongodb.replicaset}")
	private String replicaset;
	
	@Value("${spring.data.mongodb.username}")
	private String username;
	
	@Value("${spring.data.mongodb.password}")
	private String password;
	
	protected String getDatabaseName()
	{
	    return this.database;
		
	}

	@Bean
	public MongoClient mongoClient() {

		MongoClientURI uri = null;
		if (replicaset==null || replicaset.trim().length() == 0 ) {
			uri = new MongoClientURI("mongodb://"+username+":"+password+"@"+host+"/"+getDatabaseName());
		}else{

			String suri = "mongodb://"+username+":"+password+"@"+host+"/"+getDatabaseName()+"?replicaSet="+replicaset+"&readPreference=nearest";
			uri = new MongoClientURI(suri);
		}
		        
        MongoClient client = new MongoClient(uri);
        return client;
	}
	@Bean
	public MongoDbFactory mongoDbFactory() {
		MongoDbFactory factory = new SimpleMongoDbFactory(mongoClient(), getDatabaseName());
		return factory;
	}
	@Bean
	public MongoTemplate mongoTemplate() {
		MongoTemplate template = new MongoTemplate(mongoDbFactory());
		return template;
	}
	
	
}
	
	
	
	
