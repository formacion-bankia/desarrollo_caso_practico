package com.bankia.formacion.avanzada.mscs.mongo.entities;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="RQCTARJETA_CLIENTEFLIP_V1")
public class TarjetaClienteFlipEntity extends TarjetaClienteEntity {
	public TarjetaClienteFlipEntity(TarjetaClienteEntity tarjetaClienteEntity) {
		super(tarjetaClienteEntity);
	}
	
	public TarjetaClienteFlipEntity() {
		super();
	}
}
