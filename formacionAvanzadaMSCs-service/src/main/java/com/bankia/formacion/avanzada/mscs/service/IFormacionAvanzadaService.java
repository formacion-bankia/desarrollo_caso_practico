package com.bankia.formacion.avanzada.mscs.service;

import javax.validation.Valid;

import com.bankia.formacion.avanzada.mscs.api.request.BBDDNoRelacionalRequest;
import com.bankia.formacion.avanzada.mscs.api.request.BBDDRelacionalRequest;
import com.bankia.formacion.avanzada.mscs.api.request.InvocacionMSCRequest;
import com.bankia.formacion.avanzada.mscs.api.request.InvocacionSNGRequest;
import com.bankia.formacion.avanzada.mscs.api.response.BBDDNoRelacionalResponse;
import com.bankia.formacion.avanzada.mscs.api.response.BBDDRelacionalResponse;
import com.bankia.formacion.avanzada.mscs.api.response.CabecerasNegocioResponse;
import com.bankia.formacion.avanzada.mscs.api.response.InvocacionMSCResponse;
import com.bankia.formacion.avanzada.mscs.api.response.InvocacionSNGResponse;
import com.bankia.formacion.avanzada.mscs.exceptionutil.exceptions.extended.MyCustomConflictException;

public interface IFormacionAvanzadaService {

	String doSomething();

	CabecerasNegocioResponse getLecturaCabeceras();

	void setResgistroROS();

	void setRegistroBBDDRelacional(@Valid BBDDRelacionalRequest request) throws MyCustomConflictException;

	BBDDRelacionalResponse getRegistroBBDDRelacional(String idSolicitud) throws MyCustomConflictException;

	void setRegistroBBDDRNoelacional(@Valid BBDDNoRelacionalRequest request) throws MyCustomConflictException;

	BBDDNoRelacionalResponse getRegistroBBDDNoRelacional(String numTarjeta) throws MyCustomConflictException;

	InvocacionSNGResponse invocacionSNG(InvocacionSNGRequest request) throws MyCustomConflictException;

	InvocacionMSCResponse invocacionMSC(InvocacionMSCRequest request) throws MyCustomConflictException;



}
