package com.bankia.formacion.avanzada.mscs.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;
import org.yaml.snakeyaml.representer.BaseRepresenter;

import com.bankia.aq.msa.common.api.BaseResponse;
import com.bankia.aq.msa.common.context.SNHeadersContext;
import com.bankia.aq.msa.common.context.SNHeadersEnum;
import com.bankia.aq.msa.common.log.filter.service.CustomInfoService;
import com.bankia.aq.msa.invocation.service.InvocationService;
import com.bankia.formacion.avanzada.mscs.api.request.BBDDNoRelacionalRequest;
import com.bankia.formacion.avanzada.mscs.api.request.BBDDRelacionalRequest;
import com.bankia.formacion.avanzada.mscs.api.request.InvocacionMSCRequest;
import com.bankia.formacion.avanzada.mscs.api.request.InvocacionSNGRequest;
import com.bankia.formacion.avanzada.mscs.api.response.BBDDNoRelacionalResponse;
import com.bankia.formacion.avanzada.mscs.api.response.BBDDRelacionalResponse;
import com.bankia.formacion.avanzada.mscs.api.response.CabecerasNegocioResponse;
import com.bankia.formacion.avanzada.mscs.api.response.InvocacionMSCResponse;
import com.bankia.formacion.avanzada.mscs.api.response.InvocacionSNGResponse;
import com.bankia.formacion.avanzada.mscs.exceptionutil.exceptions.extended.MyCustomConflictException;
import com.bankia.formacion.avanzada.mscs.mongo.entities.TarjetaClienteEntity;
import com.bankia.formacion.avanzada.mscs.mongo.reporitories.TarjetaClienteRepository;
import com.bankia.formacion.avanzada.mscs.persistence.entities.GestionSCAEntity;
import com.bankia.formacion.avanzada.mscs.persistence.repositories.GestionSCARepository;
import com.bankia.formacion.avanzada.mscs.service.IFormacionAvanzadaService;
import com.bankia.msc.aq.seguridad.api.request.RequestReglaAutenticacionExterna;
import com.bankia.msc.aq.seguridad.api.response.ResponseReglaAutenticacionExterna;
import com.bankia.sn.dto.EnviarNotificacionesSNG.PeticionEnviarNotificacionesSNG_v1;
import com.bankia.sn.dto.EnviarNotificacionesSNG.RespuestaEnviarNotificacionesSNG_v1;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
@RefreshScope
public class FormacionAvanzadaServiceImpl implements IFormacionAvanzadaService {

	private static final Logger log = LoggerFactory.getLogger(FormacionAvanzadaServiceImpl.class);
	
	@Autowired
	CustomInfoService customInfoService;
	
	@Autowired
	private GestionSCARepository gestionSCARepository;
	
	@Autowired
	private InvocationService invocationService;
	
	@Autowired
	private TarjetaClienteRepository tarjetaClienteRepository;
	
	private static final String NOMBRE_SERVICIO_NOTIFICACION_PUSH = "EnviarNotificacionesSNG";
	private static final String VERSION_SERVICIO_NOTIFICACION_PUSH = "1.0";
	
	private static final String NOMBRE_SERVICIO_INVOKE_CHALLENGE = "CrearReglaAutenticacionExterna"; 
	private static final String VERSION_SERVICIO_INVOKE_CHALLENGE = "v1";
	
	

	@Override
	public String doSomething() {
		log.debug("This method is an example");
		
        String response = "Lógica negocio";

        
        return response;
	}

	@Override
	public CabecerasNegocioResponse getLecturaCabeceras() {
		CabecerasNegocioResponse repsonse = new CabecerasNegocioResponse();
		Map<String, String> cabecerasNegocio = new HashMap<String, String>();
		
		for(SNHeadersEnum key: SNHeadersEnum.values()){
			cabecerasNegocio.put(key.getHeaderName(), SNHeadersContext.getInstance().getHeaderValue(key));
		}
		repsonse.setCabecerasNegocio(cabecerasNegocio);
		
		return repsonse;
	}

	@Override
	public void setResgistroROS() {
		JSONObject object=new JSONObject();
		object.put("formacionAvanzadaInicio", "primer dato");
		customInfoService.createOrReplaceCustomInfo(object);
		
		object=new JSONObject();
		object.put("formacionAvanzadaOtro", "otro dato");
		customInfoService.appendToCustomInfo(object);
		
	}

	@Override
	public void setRegistroBBDDRelacional(@Valid BBDDRelacionalRequest request) throws MyCustomConflictException {
		
		GestionSCAEntity gestionSCAEntity = gestionSCARepository.findByIdSolicitud(request.getOperationId());
		if(gestionSCAEntity != null) {
			// La solicitud ya existe 
			log.error(this.getClass().getName()  + "setRegistroBBDDRelacional - La solicitud ya existe (opId:{})", request.getOperationId());
			throw new MyCustomConflictException("La solicitud ya existe (opId: " + request.getOperationId() + ")");
		}
		
		gestionSCAEntity = new GestionSCAEntity();
		gestionSCAEntity.setFecha(request.getFecha());
		gestionSCAEntity.setHora(request.getHora());
		gestionSCAEntity.setPan(request.getPan());
		gestionSCAEntity.setExpDate(request.getExpDate());
		gestionSCAEntity.setPurchaseAmount(new BigDecimal(request.getPurchaseAmount()));
		gestionSCAEntity.setPurchaseCurrency(request.getPurchaseCurrency());
		gestionSCAEntity.setMerchantName(request.getMerchantName());
		gestionSCAEntity.setIdSolicitud(request.getOperationId());
		gestionSCAEntity.setFechaSolicitud(new Date());
		gestionSCAEntity.setFinalizado('N');
		gestionSCAEntity.setApplication(request.getApplication());
		gestionSCAEntity.setProducto(request.getBkProductoPersTj());
		gestionSCAEntity.setOffset(request.getBkOffsetTj());
		gestionSCAEntity.setTipoTarjeta(request.getBkTipoTj());
		gestionSCAEntity.setEstadoTarjeta(request.getBkEstadoTj());
		gestionSCAEntity.setTipoSolicitud('1');
		ObjectMapper objectMapper = new ObjectMapper();
		try{
			gestionSCAEntity.setSolicitud(objectMapper.writeValueAsString(request));
		}catch(Exception e){
			log.error(this.getClass().getName()  + "setRegistroBBDDRelacional - No se puede guardar la solicitud (error:{})", e);
		}
		
		gestionSCARepository.save(gestionSCAEntity);
	}

	@Override
	public BBDDRelacionalResponse getRegistroBBDDRelacional(String idSolicitud) throws MyCustomConflictException {
		GestionSCAEntity gestionSCAEntity = gestionSCARepository.findByIdSolicitud(idSolicitud);
		if(gestionSCAEntity == null) {
			// La solicitud no existe 
			log.error(this.getClass().getName()  + "getRegistroBBDDRelacional - La solicitud no existe (opId:{})", idSolicitud);
			throw new MyCustomConflictException("La solicitud no existe (opId: " + idSolicitud + ")");
		}else{
			BBDDRelacionalResponse response = new BBDDRelacionalResponse();
			response.setFecha(gestionSCAEntity.getFecha());
			response.setHora(gestionSCAEntity.getHora());
			response.setPan(gestionSCAEntity.getPan());
			response.setExpDate(gestionSCAEntity.getExpDate());
			response.setPurchaseAmount(String.valueOf(gestionSCAEntity.getPurchaseAmount()));
			response.setPurchaseCurrency(gestionSCAEntity.getPurchaseCurrency());
			response.setMerchantName(gestionSCAEntity.getMerchantName());
			response.setOperationId(gestionSCAEntity.getIdSolicitud());
			response.setFechaSolicitud(gestionSCAEntity.getFechaSolicitud());
			response.setFinalizado(gestionSCAEntity.getFinalizado());
			response.setApplication(gestionSCAEntity.getApplication());
			response.setBkProductoPersTj(gestionSCAEntity.getProducto());
			response.setBkOffsetTj(gestionSCAEntity.getOffset());
			response.setBkTipoTj(gestionSCAEntity.getTipoTarjeta());	
			response.setBkEstadoTj(gestionSCAEntity.getEstadoTarjeta());
	
			return response;
		}
	}

	@Override
	public void setRegistroBBDDRNoelacional(@Valid BBDDNoRelacionalRequest request)
			throws MyCustomConflictException {
		List<TarjetaClienteEntity> tarjetasClienteEntity = tarjetaClienteRepository.findByTarjeta(request.getNumTarjeta());
		if(tarjetasClienteEntity != null && !tarjetasClienteEntity.isEmpty()){
			log.error(this.getClass().getName()  + ".setRegistroBBDDRNoelacional - La tarjeta ya existe (tarjeta:{})", request.getNumTarjeta());
			throw new MyCustomConflictException("La tarjeta ya existe (tarjeta: " + request.getNumTarjeta() + ")");
		}
		TarjetaClienteEntity tarjetaClienteEntity = new TarjetaClienteEntity();
		tarjetaClienteEntity.setCo_persona(request.getIdCliente());
		tarjetaClienteEntity.setNu_contrato(request.getNumContrato());
		tarjetaClienteEntity.setNu_documento(request.getNumDocumento());
		tarjetaClienteEntity.setNu_tarjeta(request.getNumTarjeta());
		
		tarjetaClienteRepository.insert(tarjetaClienteEntity);
		
	}

	@Override
	public BBDDNoRelacionalResponse getRegistroBBDDNoRelacional(String numTarjeta) throws MyCustomConflictException {
		List<TarjetaClienteEntity> tarjetasClienteEntity = tarjetaClienteRepository.findByTarjeta(numTarjeta);
		if(tarjetasClienteEntity == null || tarjetasClienteEntity.isEmpty()){
			log.error(this.getClass().getName()  + ".setRegistroBBDDRNoelacional - La tarjeta no existe (tarjeta:{})", numTarjeta);
			throw new MyCustomConflictException("La tarjeta no existe (tarjeta: " + numTarjeta + ")");
		}else{
			TarjetaClienteEntity tarjetaClienteEntity = tarjetasClienteEntity.get(0);
			BBDDNoRelacionalResponse response = new BBDDNoRelacionalResponse();
			response.setIdCliente(tarjetaClienteEntity.getCo_persona());
			response.setNumContrato(tarjetaClienteEntity.getNu_contrato());
			response.setNumDcumento(tarjetaClienteEntity.getNu_documento());
			response.setNumTarjeta(tarjetaClienteEntity.getNu_tarjeta());
			return response;
		}
	}

	@Override
	public InvocacionSNGResponse invocacionSNG(InvocacionSNGRequest request) throws MyCustomConflictException {
		PeticionEnviarNotificacionesSNG_v1 peticionEnviarNotificaciones = new PeticionEnviarNotificacionesSNG_v1();
		peticionEnviarNotificaciones.setIdentificadorMensaje(request.getIdentificadorMensaje());
		peticionEnviarNotificaciones.setIndicadorBusquedaDatosBasicos(request.getIndicadorBusquedaDatosBasicos());
		peticionEnviarNotificaciones.setIdentificadorCliente(request.getIdentificadorCliente());
		peticionEnviarNotificaciones.setIndicadorEnvioSMS(request.getIndicadorEnvioSMS());
		peticionEnviarNotificaciones.setIndicadorEnvioEmail(request.getIndicadorEnvioEmail());
		peticionEnviarNotificaciones.setIndicadorEnvioPush(request.getIndicadorEnvioPush());
		peticionEnviarNotificaciones.setIndicadorPublicidad(request.getIndicadorPublicidad());
		peticionEnviarNotificaciones.setCodigoIdioma(request.getCodigoIdioma());
		peticionEnviarNotificaciones.setIndicadorEnviarAdjunto(request.getIndicadorEnviarAdjunto());
		peticionEnviarNotificaciones.setIdentificadorOperacion(request.getIdentificadorOperacion());
		
		BaseResponse<RespuestaEnviarNotificacionesSNG_v1> baseResponse = invocationService
				.invoke(null, peticionEnviarNotificaciones,
						NOMBRE_SERVICIO_NOTIFICACION_PUSH,
						VERSION_SERVICIO_NOTIFICACION_PUSH, null, null,
						RespuestaEnviarNotificacionesSNG_v1.class);
		if(!baseResponse.isSuccess()){
			log.error(this.getClass().getName()  + ".invocacionSNG - error al invocar al SNG");
			throw new MyCustomConflictException(" Error al invocar al SNG: " + baseResponse.getException().getMessage());

		}
		InvocacionSNGResponse response = new InvocacionSNGResponse();
		response.setIdentificadorMensajeEnviado(baseResponse.getParametrosSalida().getIdentificadorMensajeEnviado());
		return response;
	}

	@Override
	public InvocacionMSCResponse invocacionMSC(InvocacionMSCRequest request)
			throws MyCustomConflictException {
		RequestReglaAutenticacionExterna requestReglaAutenticacionExterna = new RequestReglaAutenticacionExterna();
		requestReglaAutenticacionExterna.setFechaCaducidad(request.getFechaCaducidad());
		requestReglaAutenticacionExterna.setIdOperacion(request.getIdOperacion());
		requestReglaAutenticacionExterna.setImporteOperacion(request.getImporteOperacion());
		requestReglaAutenticacionExterna.setMonedaOperacion(request.getMonedaOperacion());
		requestReglaAutenticacionExterna.setNombreComercio(request.getNombreComercio());
		requestReglaAutenticacionExterna.setOffsetPan(request.getOffsetPan());
		requestReglaAutenticacionExterna.setPan(request.getPan());
		
		BaseResponse<ResponseReglaAutenticacionExterna> baseResponse = invocationService
				.invoke(null, requestReglaAutenticacionExterna,
						NOMBRE_SERVICIO_INVOKE_CHALLENGE,
						VERSION_SERVICIO_INVOKE_CHALLENGE, null, null,
						ResponseReglaAutenticacionExterna.class);
		
		if(!baseResponse.isSuccess()){
			log.error(this.getClass().getName()  + ".invocacionMSC - error al invocar al MSC");
			throw new MyCustomConflictException(" Error al invocar al MSC: " + baseResponse.getException().getMessage());

		}else{
			ResponseReglaAutenticacionExterna responseReglaAutenticacionExterna = baseResponse.getParametrosSalida();
			InvocacionMSCResponse response = new InvocacionMSCResponse();
			response.setMensajeAutenticacion(responseReglaAutenticacionExterna.getMensajeAutenticacion());
			response.setOperacion(responseReglaAutenticacionExterna.getOperacion());
			response.setPosicion1(responseReglaAutenticacionExterna.getPosicion1());
			response.setPosicion2(responseReglaAutenticacionExterna.getPosicion2());
			response.setResultadoAutenticacion(responseReglaAutenticacionExterna.getResultadoAutenticacion());
			response.setSumando(responseReglaAutenticacionExterna.getSumando());
			
			return response;
		}
		
		
		
	}
}
