package com.bankia.formacion.avanzada.mscs.mongo.entities;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="CATALOGO_V2")
public class CatalogoEntity
{
  private String maestra;
  private String flip;
  private String flop;
  private int idlog;
  private String instancia;
  private String indice;
  private String SAP;
  private String fecha;
  private String responsable;
  private String ordenacion;
  
  public String getMaestra()
  {
    return this.maestra;
  }
  
  public void setMaestra(String maestra)
  {
    this.maestra = maestra;
  }
  
  public String getFlip()
  {
    return this.flip;
  }
  
  public void setFlip(String flip)
  {
    this.flip = flip;
  }
  
  public String getFlop()
  {
    return this.flop;
  }
  
  public void setFlop(String flop)
  {
    this.flop = flop;
  }
  
  public int getIdlog()
  {
    return this.idlog;
  }
  
  public void setIdlog(int idlog)
  {
    this.idlog = idlog;
  }
  
  public String getInstancia()
  {
    return this.instancia;
  }
  
  public void setInstancia(String instancia)
  {
    this.instancia = instancia;
  }
  
  public String getIndice()
  {
    return this.indice;
  }
  
  public void setIndice(String indice)
  {
    this.indice = indice;
  }
  
  public String getSAP()
  {
    return this.SAP;
  }
  
  public void setSAP(String sAP)
  {
    this.SAP = sAP;
  }
  
  public String getFecha()
  {
    return this.fecha;
  }
  
  public void setFecha(String fecha)
  {
    this.fecha = fecha;
  }
  
  public String getResponsable()
  {
    return this.responsable;
  }
  
  public void setResponsable(String responsable)
  {
    this.responsable = responsable;
  }
  
  public String getOrdenacion()
  {
    return this.ordenacion;
  }
  
  public void setOrdenacion(String ordenacion)
  {
    this.ordenacion = ordenacion;
  }
  
  public String toString()
  {
    return "Catalogo [maestra=" + this.maestra + ", flip=" + this.flip + ", flop=" + this.flop + ", idlog=" + this.idlog + ", instancia=" + this.instancia + ", indice=" + this.indice + ", SAP=" + this.SAP + ", fecha=" + this.fecha + ", responsable=" + this.responsable + ", ordenacion=" + this.ordenacion + "]";
  }
}
