
package com.bankia.formacion.avanzada.mscs.configuration;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableJpaAuditing
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "entitymanagerfactoryGestionSCA", transactionManagerRef = "transactionmanagerGestionSCA",
basePackages = {"com.bankia.formacion.avanzada.mscs.persistence.repositories"})
@PropertySource("classpath:common-database.properties")
public class DataBaseConfiguration {

	private static final Logger log = LoggerFactory.getLogger(DataBaseConfiguration.class);

    /** The Constant DB_DIALECT. */
    private static final String GESTIONSCADB_DIALECT = "gestion.jpa.database-platform";

    /** The Constant DB_DEFAULT_SCHEMA. */
    private static final String GESTIONSCADB_DEFAULT_SCHEMA = "gestion.datasource.schema";

    /** The Constant DB_HIBERNATE_HBM2DDL_AUTO. */
    private static final String GESTIONSCADB_HIBERNATE_HBM2DDL_AUTO = "gestion.jpa.hibernate.ddl-auto";

    /** The Constant DB_SHOW_SQL. */
    private static final String GESTIONSCADB_SHOW_SQL = "gestion.jpa.show-sql";
	
    @Primary
    @Bean(name = "datasourceGestionSCA")
    @ConfigurationProperties(prefix = "gestion.datasource")
	public DataSource dataSource() {
        return DataSourceBuilder.create().build();
    }
	@Primary
	@Bean(name = "entitymanagerfactoryGestionSCA")
	public LocalContainerEntityManagerFactoryBean entityManagerFactory(final EntityManagerFactoryBuilder builder,
			@Qualifier("datasourceGestionSCA") final DataSource dataSource, final Environment env) {

		Map<String, String> props = new HashMap<String, String>();
		props.put("hibernate.dialect", env.getRequiredProperty(GESTIONSCADB_DIALECT));
		props.put("hibernate.hbm2ddl.auto", env.getRequiredProperty(GESTIONSCADB_HIBERNATE_HBM2DDL_AUTO));
		props.put("hibernate.show_sql", env.getRequiredProperty(GESTIONSCADB_SHOW_SQL));
		props.put("hibernate.default_schema", env.getRequiredProperty(GESTIONSCADB_DEFAULT_SCHEMA));

		log.debug("Loading GestionSCA database properties ... {}", props);

		return builder.dataSource(dataSource).properties(props).packages("com.bankia.formacion.avanzada.mscs.persistence.entities")
				.persistenceUnit("gestionSCApunit").build();
    	}    
        
    @Primary
	@Bean(name = "transactionmanagerGestionSCA")
	public PlatformTransactionManager transactionManager(
			@Qualifier("entitymanagerfactoryGestionSCA") final EntityManagerFactory entityManagerFactory) {
		return new JpaTransactionManager(entityManagerFactory);

	}

}