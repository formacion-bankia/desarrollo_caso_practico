package com.bankia.formacion.avanzada.mscs.persistence.entities;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.domain.Persistable;



@Entity
@Table(name = "GESTION_SCA")
public class GestionSCAEntity implements Persistable<Long> {

	@Id
    @GenericGenerator(
            name = "GESTION_SCA_SEQ",
            strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = {
                    @Parameter(name = "sequence_name", value = "GESTION_SCA_SEQ"),
                    @Parameter(name = "initial_value", value = "1"),
                    @Parameter(name = "increment_size", value = "1")
            }
    )              
    @GeneratedValue(generator = "GESTION_SCA_SEQ")
    @Column(name = "ID_GESTION_SCA")
	private Long idGestionSca;
	
	@Column(name = "CLIENTE")
	private String cliente = null;
	
	@Column(name = "APPLICATION")
	private String application = null;

	@Column(name = "CONTRATO")
	private String contrato = null;
	
	@Column(name = "DOCUMENTO")
	private String documento = null;
	
	@Column(name = "ESTADO")
	private String status = null;
	
	@Column(name = "INFO_AUTH_METHOD")
	private String infoAuthMethod = null;
	
	@Column(name = "FECHA")
	private Date fecha = null;
	
	@Column(name = "HORA")
	private String hora = null;
	
	@Column(name = "PAN")
	private String pan = null;
	
	@Column(name = "EXP_DATE")
	private String expDate = null;
	
	@Column(name = "PURCHASE_AMOUNT")
	private BigDecimal purchaseAmount = null;
	
	@Column(name = "PURCHASE_CURRENCY")
	private String purchaseCurrency = null;
	
	@Column(name = "MERCHANT_NAME")
	private String merchantName = null;
	
	@Column(name = "ID_SOLICITUD")
	private String idSolicitud = null;
	
	@Column(name = "FE_SOLICITUD")
	@CreatedDate
	private Date fechaSolicitud;
	
	@Column(name = "TIPO_OTP")
	private String tipoOTP = null;
	
	@Column(name = "DATOS_OTP")
	private String datos = null;
	
	@Column(name = "CLAVE")
	private String clave = null;
	
	@Column(name = "IN_FINALIZADO")
	private Character finalizado;
	
	@Column(name = "TIPO_TARJETA")
	private String tipoTarjeta;

	@Column(name = "ESTADO_TARJETA")
	private String estadoTarjeta;
	
	@Column(name = "PRODUCTO")
	private String producto;
	
	@Column(name = "OFFSET")
	private String offset;
	
	@Column(name = "TIPO_SOLICITUD")
	private Character tipoSolicitud;
	
	@Column(name = "SOLICITUD")
	private String solicitud;

	@Override
	public Long getId() {
		return idGestionSca;
	}

	@Override
	public boolean isNew() {
		return false;
	}

	public Long getIdGestionSca() {
		return idGestionSca;
	}

	public void setIdGestionSca(Long idGestionSca) {
		this.idGestionSca = idGestionSca;
	}

	public String getCliente() {
		return cliente;
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	public String getApplication() {
		return application;
	}

	public void setApplication(String application) {
		this.application = application;
	}

	public String getContrato() {
		return contrato;
	}

	public void setContrato(String contrato) {
		this.contrato = contrato;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getInfoAuthMethod() {
		return infoAuthMethod;
	}

	public void setInfoAuthMethod(String infoAuthMethod) {
		this.infoAuthMethod = infoAuthMethod;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public String getExpDate() {
		return expDate;
	}

	public void setExpDate(String expDate) {
		this.expDate = expDate;
	}

	public BigDecimal getPurchaseAmount() {
		return purchaseAmount;
	}

	public void setPurchaseAmount(BigDecimal purchaseAmount) {
		this.purchaseAmount = purchaseAmount;
	}

	public String getPurchaseCurrency() {
		return purchaseCurrency;
	}

	public void setPurchaseCurrency(String purchaseCurrency) {
		this.purchaseCurrency = purchaseCurrency;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public String getIdSolicitud() {
		return idSolicitud;
	}

	public void setIdSolicitud(String idSolicitud) {
		this.idSolicitud = idSolicitud;
	}

	public Date getFechaSolicitud() {
		return fechaSolicitud;
	}

	public void setFechaSolicitud(Date fechaSolicitud) {
		this.fechaSolicitud = fechaSolicitud;
	}

	public String getTipoOTP() {
		return tipoOTP;
	}

	public void setTipoOTP(String tipoOTP) {
		this.tipoOTP = tipoOTP;
	}

	public String getDatos() {
		return datos;
	}

	public void setDatos(String datos) {
		this.datos = datos;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public Character getFinalizado() {
		return finalizado;
	}

	public void setFinalizado(Character finalizado) {
		this.finalizado = finalizado;
	}

	public String getTipoTarjeta() {
		return tipoTarjeta;
	}

	public void setTipoTarjeta(String tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}

	public String getEstadoTarjeta() {
		return estadoTarjeta;
	}

	public void setEstadoTarjeta(String estadoTarjeta) {
		this.estadoTarjeta = estadoTarjeta;
	}

	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public String getOffset() {
		return offset;
	}

	public void setOffset(String offset) {
		this.offset = offset;
	}

	public Character getTipoSolicitud() {
		return tipoSolicitud;
	}

	public void setTipoSolicitud(Character tipoSolicitud) {
		this.tipoSolicitud = tipoSolicitud;
	}

	public String getSolicitud() {
		return solicitud;
	}

	public void setSolicitud(String solicitud) {
		this.solicitud = solicitud;
	}
}