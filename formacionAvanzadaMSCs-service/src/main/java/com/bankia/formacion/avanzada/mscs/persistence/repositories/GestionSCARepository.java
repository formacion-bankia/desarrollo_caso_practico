package com.bankia.formacion.avanzada.mscs.persistence.repositories;
import org.springframework.data.jpa.repository.JpaRepository;
import com.bankia.formacion.avanzada.mscs.persistence.entities.GestionSCAEntity;

public interface GestionSCARepository extends JpaRepository<GestionSCAEntity, Long>{
	GestionSCAEntity findByIdSolicitud(String operationId);

}