package com.bankia.formacion.avanzada.mscs.exceptionutil.exceptions.extended;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.bankia.formacion.avanzada.mscs.exceptionutil.exceptions.BaseException;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class OtherCustomConflictException extends BaseException{

	private static final long serialVersionUID = -7408570608698100557L;
	private static final String errorCode = "Error custom";
		
	public OtherCustomConflictException(String message,String errorCode) {
		super(message,errorCode);
	}

	
	
}
