package com.bankia.formacion.avanzada.mscs.kafka;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;
import org.springframework.stereotype.Component;

@Component
public interface KafkaInterface {
	String INPUT_TOPIC = "FORMACION_AVANZADA";

	@Input( INPUT_TOPIC )
	SubscribableChannel inboundFormacionAvanzada();
}
