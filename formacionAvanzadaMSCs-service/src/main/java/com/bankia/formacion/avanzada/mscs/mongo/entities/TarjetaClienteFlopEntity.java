package com.bankia.formacion.avanzada.mscs.mongo.entities;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="RQCTARJETA_CLIENTEFLOP_V1")
public class TarjetaClienteFlopEntity extends TarjetaClienteEntity{
	public TarjetaClienteFlopEntity(TarjetaClienteEntity tarjetaClienteEntity) {
		super(tarjetaClienteEntity);
	}
	
	public TarjetaClienteFlopEntity() {
		super();
	}
}
