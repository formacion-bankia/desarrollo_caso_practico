package com.bankia.formacion.avanzada.mscs.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bankia.aq.msa.common.api.BaseResponse;
import com.bankia.formacion.avanzada.mscs.api.request.BBDDNoRelacionalRequest;
import com.bankia.formacion.avanzada.mscs.api.request.BBDDRelacionalRequest;
import com.bankia.formacion.avanzada.mscs.api.request.InvocacionMSCRequest;
import com.bankia.formacion.avanzada.mscs.api.request.InvocacionSNGRequest;
import com.bankia.formacion.avanzada.mscs.api.response.BBDDNoRelacionalResponse;
import com.bankia.formacion.avanzada.mscs.api.response.BBDDRelacionalResponse;
import com.bankia.formacion.avanzada.mscs.api.response.CabecerasNegocioResponse;
import com.bankia.formacion.avanzada.mscs.api.response.FormacionAvanzadaResponse;
import com.bankia.formacion.avanzada.mscs.api.response.InvocacionMSCResponse;
import com.bankia.formacion.avanzada.mscs.api.response.InvocacionSNGResponse;
import com.bankia.formacion.avanzada.mscs.exceptionutil.exceptions.extended.MyCustomConflictException;
import com.bankia.formacion.avanzada.mscs.service.IFormacionAvanzadaService;

@RestController
//@EnableBinding(KafkaInterface.class)
public class FormacionAvanzadaController extends BaseController {
		
    private static final Logger log = LoggerFactory.getLogger(FormacionAvanzadaController.class);

    @Autowired
    private IFormacionAvanzadaService service;

    @GetMapping(path = "/myCustomPath", consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = {
            MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<BaseResponse<FormacionAvanzadaResponse>> get() {
        MDC.put(MDC_TRACKING_TAG, "FormacionAvanzadaController.get");
        log.debug(this.getClass().getName() + "Entering get()");

        FormacionAvanzadaResponse response = null;//service.doSomething();

        BaseResponse<FormacionAvanzadaResponse> baseResponse = new BaseResponse<>();
        baseResponse.setParametrosSalida(response);

        return ResponseEntity.ok(baseResponse);
    }
    
   // @StreamListener(KafkaInterface.INPUT_TOPIC)
    public void handleKafkaMsg(@Payload String payload){
    	log.info("Mensaje Kafka recibido: {}",  payload);
    }
    
    
    @GetMapping(path = "/lecturaCabeceras", consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = {
            MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<BaseResponse<CabecerasNegocioResponse>> getLecturaCabeceras() {
        MDC.put(MDC_TRACKING_TAG, "FormacionAvanzadaController.getLecturaCabeceras");
        log.debug(this.getClass().getName() + "Entering getLecturaCabeceras()");

        CabecerasNegocioResponse response = service.getLecturaCabeceras();

        BaseResponse<CabecerasNegocioResponse> baseResponse = new BaseResponse<>();
        baseResponse.setParametrosSalida(response);

        return ResponseEntity.ok(baseResponse);
    }
    
    @PostMapping(path = "/registroROS", consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = {
            MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<Void> setResgistroROS() {
        MDC.put(MDC_TRACKING_TAG, "FormacionAvanzadaController.setResgistroROS");
        log.debug(this.getClass().getName() + "Entering setResgistroROS()");

        service.setResgistroROS();

		return new  ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }

    @PostMapping(path = "/registroBBDDRelacional", consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = {
            MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<Void> setRegistroBBDDRelacional(@Valid @RequestBody BBDDRelacionalRequest request) throws MyCustomConflictException {
        MDC.put(MDC_TRACKING_TAG, "FormacionAvanzadaController.setRegistroBBDDRelacional");
        log.debug(this.getClass().getName() + "Entering setRegistroBBDDRelacional()");

        service.setRegistroBBDDRelacional(request);

        return new  ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }

    @GetMapping(path = "/consultaBBDDRelacional/{idSolicitud}", consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = {
            MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<BaseResponse<BBDDRelacionalResponse>> getRegistroBBDDRelacional(@PathVariable("idSolicitud") String idSolicitud) throws MyCustomConflictException {
        MDC.put(MDC_TRACKING_TAG, "FormacionAvanzadaController.getRegistroBBDDRelacional");
        log.debug(this.getClass().getName() + "Entering getRegistroBBDDRelacional()");

        BBDDRelacionalResponse response = service.getRegistroBBDDRelacional(idSolicitud);

        BaseResponse<BBDDRelacionalResponse> baseResponse = new BaseResponse<>();
        baseResponse.setParametrosSalida(response);

        return ResponseEntity.ok(baseResponse);
    }
    
    @PostMapping(path = "/registroBBDDNoRelacional", consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = {
            MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<Void> setRegistroBBDDNoRelacional(@Valid @RequestBody BBDDNoRelacionalRequest request) throws MyCustomConflictException {
        MDC.put(MDC_TRACKING_TAG, "FormacionAvanzadaController.setRegistroBBDDNoRelacional");
        log.debug(this.getClass().getName() + "Entering setRegistroBBDDNoRelacional()");

        service.setRegistroBBDDRNoelacional(request);

        return new  ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }

    @GetMapping(path = "/consultaBBDDNoRelacional/{numTarjeta}", consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = {
            MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<BaseResponse<BBDDNoRelacionalResponse>> getRegistroBBDDNoRelacional(@PathVariable("numTarjeta") String numTarjeta) throws MyCustomConflictException {
        MDC.put(MDC_TRACKING_TAG, "FormacionAvanzadaController.getRegistroBBDDNoRelacional");
        log.debug(this.getClass().getName() + "Entering getRegistroBBDDNoRelacional()");

        BBDDNoRelacionalResponse response = service.getRegistroBBDDNoRelacional(numTarjeta);

        BaseResponse<BBDDNoRelacionalResponse> baseResponse = new BaseResponse<>();
        baseResponse.setParametrosSalida(response);

        return ResponseEntity.ok(baseResponse);
    }
    
    @PostMapping(path = "/invocacionSNG", consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = {
            MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<BaseResponse<InvocacionSNGResponse>> invocacionSNG(@RequestBody InvocacionSNGRequest request) throws MyCustomConflictException {
        MDC.put(MDC_TRACKING_TAG, "FormacionAvanzadaController.invocacionSNG");
        log.debug(this.getClass().getName() + "Entering invocacionSNG()");

        InvocacionSNGResponse response = service.invocacionSNG(request);

        BaseResponse<InvocacionSNGResponse> baseResponse = new BaseResponse<>();
        baseResponse.setParametrosSalida(response);

        return ResponseEntity.ok(baseResponse);
    }
    
    @PostMapping(path = "/invocacionMSC", consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = {
            MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<BaseResponse<InvocacionMSCResponse>> invocacionMSC(@RequestBody InvocacionMSCRequest request) throws MyCustomConflictException {
        MDC.put(MDC_TRACKING_TAG, "FormacionAvanzadaController.invocacionMSC");
        log.debug(this.getClass().getName() + "Entering invocacionMSC()");

        InvocacionMSCResponse response = service.invocacionMSC(request);

        BaseResponse<InvocacionMSCResponse> baseResponse = new BaseResponse<>();
        baseResponse.setParametrosSalida(response);

        return ResponseEntity.ok(baseResponse);
    }
}
