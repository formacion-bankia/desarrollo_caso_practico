package com.bankia.formacion.avanzada.mscs.exceptionutil.exceptions;

import com.bankia.aq.msa.common.api.ExceptionMessage;

public class BaseException extends Exception{
	
	private static final long serialVersionUID = -1671040781272671198L;

	private ExceptionMessage exceptionMessage;

	public BaseException(String message) {
		super(message);
		exceptionMessage = new ExceptionMessage();
		exceptionMessage.setMessage(message);
	}

	public BaseException(String message, String errorCode) {
		super(message);
		exceptionMessage = new ExceptionMessage();
		exceptionMessage.setMessage(message);
		exceptionMessage.setErrorCode(errorCode);
	}

	public BaseException(String message, Throwable throwable) {
		super(message, throwable);
		exceptionMessage = new ExceptionMessage();
		exceptionMessage.setMessage(message);
	}
	
	public BaseException(String message, String errorCode, Throwable throwable) {
		super(message, throwable);
		exceptionMessage = new ExceptionMessage();
		exceptionMessage.setMessage(message);
		exceptionMessage.setErrorCode(errorCode);
	}

	public ExceptionMessage getExceptionMessage() {
		return exceptionMessage;
	}

	public void setExceptionMessage(ExceptionMessage exceptionMessage) {
		this.exceptionMessage = exceptionMessage;
	}
	
	
}
