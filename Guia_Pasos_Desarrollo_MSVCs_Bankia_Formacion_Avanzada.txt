1º Revisión con Arquitectura de Procesos de si funcionalidad a implementar tiene cabida bajo ecosistema de microservicios (reunión)
2º Si Arquitectura de Procesos autoriza el desarrollo de microservicios, hay que solicitar vía Perseo a Arquitectura de Desarrollo (ver ejemplo https://perseo.bankia.int/browse/ARQDESA-47987):
	- Tipo: Herramientas Ciclo Vida Desarrollo - Entorno Distribuido
	- Categoría: Alta de componente aplicativo
	- Subcategoría: Alta de componente aplicativo
	- Naturaleza
	
	la creación de los espacios/proyectos en Bitbucket y Bamboo donde estarán ubicados los repositorios Git y los planes de construcción y despliegue que se soliciten posteriormente
	
3º Realizar diseño de microservicio en Enterprise Architect (EA), teniendo claro en qué repositorio/proyecto de EA (fichero .eap) se debe realizar el diseño. Solicitar vía Perseo a Arquitectura de Desarrollo (ver ejemplo https://perseo.bankia.int/browse/ARQDESA-51217)
	- Tipo: APM
	- Categoría: Gestión de Perfiles
	- Subcategoría: Alta
	- Naturaleza: Solicitud
	¡¡IMPORTANTE!! Necesario indicar en el Perseo el nombre de proyecto en SGIA (Sistema de Gestión del Inventario de Aplicaciones) que deberá saberlo el responsable Bankia del proyecto
	
	el acceso al repositorio/proyecto de EA (fichero .eap) para poder realizar el diseño. El diseño se realizará sobre carpeta/sistema "<<Sistema MSVCs>> Nombre aplicación" que deberá existir y si no existe, intentaremos crearlo con nuestro usuario A para su creación ("Carpeta Nombre aplicación" (botón derecho del ratón)->Extensions->EA Extension for Bankia->Nuevo Sistema...->Seleccionar opción "Sistema MSVCs").
	En el caso de no tener permisos para crear la nueva carpeta/sistema "<<Sistema MSVCs>> Nombre aplicación" solicitar su creación vía Perseo a Arquitectura de Servicios (ver ejemplo https://perseo.bankia.int/browse/ARQSER-22328)
	- Tipo: Modelado y Gobierno
	- Categoría: Enterprise Architect
	- Subcategoría: Enterprise Architect
	- Naturaleza: Solicitud
	
4º	Una vez realizado el diseño en EA se solicitará su revisión a Arquitectura de Servicios vía Perseo (ver ejemplo https://perseo.bankia.int/browse/ARQSER-23009)
	- Tipo: Modelado y Gobierno
	- Categoría: Enterprise Architect
	- Subcategoría: Enterprise Architect
	- Naturaleza: Solicitud

5º Tras obtener el visto bueno del diseño del microservicio realizado en EA, con el código del Perseo de la aprobación del diseño, volveremos a solicitar vía Perseo a Arquitectura de Desarrollo la creación del respositorio Git en Bitbucket y de los planes de construcción y despliegue en Bamboo para el entorno de EPD (ver ejemplo https://perseo.bankia.int/browse/ARQDESA-50438)
	- Tipo: Herramientas Ciclo Vida Desarrollo - Entorno Distribuido
	- Categoría: Alta de componente aplicativo
	- Subcategoría: Alta de componente aplicativo
	- Naturaleza: Solicitud
	¡¡IMPORTANTE!! Hay que tener claro el valor a establecer como location (base-path del microservicio) y el nombre de la aplicación DSL en Service Desk que deberá saberlo el responsable Bankia del proyecto

6º Una vez generados el repositorio Git en Bitbucket ya podemos iniciar el desarrollo propiamente dicho del microservicio
	Los arquetipos que hay disponibles actualmente para el desarrollo de microservicios son:
	* Cuando se va a desarrollar tanto el módulo API como el módulo Service del microservicio:
	mvn archetype:generate -DarchetypeGroupId=com.bankia.aq -DarchetypeArtifactId=bankia-aq-msa-2-archetype -DarchetypeVersion=2.1.0 
	
	* Cuando se va a desarrollar el módulo Service del microservicio y el módulo API se generará con lo establecido en el diseño en EA
	mvn archetype:generate -DarchetypeGroupId=com.bankia.aq -DarchetypeArtifactId=bankia-aq-msa-2-archetype -DarchetypeVersion=2.1.0 -DeaAPI=true

7º Se realiza el desarrollo y pruebas unitarias en local del microservicio

8º Se sube el código a la rama develop del repositorio Git creado en Bitbucket

9º Se lanza el plan de construcción del microservicio creado en Bamboo

10º Se lanza el plan de despliegue del microservicio creado en Bamboo para dejar disponible para su consumo el microservicio en el entorno de EPD

11º Tras validar en EPD el correcto funcionamiento del microservicio, se procede a subir el código a la rama releascandidate del repositorio Git creado en Bitbucket

12º Se solicita a través de Perseo a Arquitectura de Desarrollo la creación de un nuevo proyecto en Clarive para el alta del nuevo contenedor
	- Tipo de petición: Herramientas Ciclo Vida Desarrollo – Entorno Distribuido
	- Categoría: Clarive
	- Subcategoría: Despliegues
	- Naturaleza: Solicitud
	- Título: Alta del Proyecto en Clarive - AP-Nombre_de_aplicación-DSL
	¡¡IMPORTANTE!! Hay que conocer el nombre de la aplicación DSL que deberá saberlo el responsable Bankia del proyecto y para poder acceder a Clarive, también será necesario solicitar vía Petición de Servicio que asignen a los usuarios A correspondientes la función de seguridad FACCCLARIVE - FUNCION ACCESO A CLARIVE
	
	